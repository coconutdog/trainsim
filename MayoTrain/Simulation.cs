﻿using System;
using System.Collections.Generic;
using System.Linq;
using CsvHelper;
using CsvHelper.Configuration;
using System.IO;
using System.Globalization;
using System.Diagnostics;

namespace MayoTrain
{
    public class TrainSim
    {

        public class TrackData
        {
            public double Start_KM { get; set; }
            public double End_KM { get; set; }
            public double Start_Z { get; set; }
            public double End_Z { get; set; }
        }

        public sealed class TrackDataMap : ClassMap<TrackData>
        {
            public TrackDataMap()
            {
                Map(m => m.Start_KM).Name("Start KM");
                Map(m => m.End_KM).Name("End KM");
                Map(m => m.Start_Z).Name("Start Z");
                Map(m => m.End_Z).Name("End Z");
            }
        }

        public class CurveData
        {
            public double Start_KM { get; set; }
            public double End_KM { get; set; }
            public double Start_R { get; set; }
            public double End_R { get; set; }
        }

        public sealed class CurveDataMap : ClassMap<CurveData>
        {
            public CurveDataMap()
            {
                Map(m => m.Start_KM).Name("Start KM");
                Map(m => m.End_KM).Name("End KM");
                Map(m => m.Start_R).Name("Curve 1 Radius");
                Map(m => m.End_R).Name("Curve 2 Radius");
            }
        }
        public class StationData
        {
            public string Station { get; set; }
            public double KM { get; set; }
            public string Stop { get; set; }
        }

        public sealed class StationDataMap : ClassMap<StationData>
        {
            public StationDataMap()
            {
                Map(m => m.Station).Name("STATION");
                Map(m => m.KM).Name("KM");
                Map(m => m.Stop).Name("STOP");
            }
        }

        public static (List<double>, List<double>) LoadTrackData(string file)
        {
            // Create empty lists to store data
            var Track_KM = new List<double>();
            var Track_Y = new List<double>(); //This can be the height(z) or the curvature (r)

            using (var reader = new StreamReader(file))
            using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
            {

                // Register Class maps (required for CSVHelper)
                csv.Context.RegisterClassMap<TrackDataMap>();
                csv.Context.RegisterClassMap<CurveDataMap>();
                var isHeader = true;

                while (csv.Read())
                {
                    // File check
                    if (isHeader)
                    {
                        csv.ReadHeader();
                        isHeader = false;
                        continue;
                    }

                    // Determine which file has been loaded and export data
                    foreach (string HeaderString in csv.HeaderRecord)
                    {
                        if (HeaderString == "Start Z")
                        {
                            var record = new TrackData();
                            var records = csv.EnumerateRecords(record);

                            // Add first KM point only.
                            // After first KM point, Start_KM is not required as previous End_KM is new Start_KM.
                            Track_KM.Add(records.First().Start_KM);
                            Track_Y.Add(records.First().Start_Z);

                            // Loop through all records and add to lists.
                            foreach (var r in records)
                            {
                                Track_KM.Add(r.Start_KM);
                                Track_Y.Add(r.Start_Z);
                            }

                        }

                        if (HeaderString.Contains("Radius"))
                        {
                            var record = new CurveData();
                            var records = csv.EnumerateRecords(record);

                            double radius = 0;

                            // Loop through all records and add to lists.
                            foreach (var r in records)
                            {
                                Debug.WriteLine(r);
                                // Determine which radius is smaller and use that figure (more conservative)
                                if (r.Start_R < r.End_R) { radius = r.Start_R; } else { radius = r.End_R; }

                                // If radius is tagent or greater than 3000m, set to 0m.
                                if (radius >= 3000) { radius = 0; }

                                Track_KM.Add(r.End_KM);
                                Track_Y.Add(radius);

                            }
                        }
                    }
                }
            }
            return (Track_KM, Track_Y);           
        }

        public static (List<string>, List<double>, List<bool>) LoadStationData(string file)
        {

            // Create empty lists to store data
            var Station = new List<string>();
            var KM = new List<double>();
            var Stop = new List<bool>();

            // Open CSV and store station, KM and stopping data into 3 lists
            using (var reader = new StreamReader(file))
            using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
            {

                // Register Class map (required for CSVHelper)
                csv.Context.RegisterClassMap<StationDataMap>();

                while (csv.Read())
                {
                    // If you're looping the rows yourself and there is a header, ReadHeader must be run.
                    csv.ReadHeader();
                    var record = new StationData();
                    var records = csv.EnumerateRecords(record);

                    // Loop through all records and add to lists.
                    foreach (var r in records)
                    {
                        Station.Add(r.Station);
                        KM.Add(r.KM);
                        // If stopping data is YES, set bool to true, otherwise set to FALSE
                        if (r.Stop == "YES") { Stop.Add(true); } else {Stop.Add(false);}
                    }
                }
            }
            return (Station, KM, Stop);
        }
    }
}
