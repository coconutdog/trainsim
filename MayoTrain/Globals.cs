﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MayoTrain
{
    public static class Globals
    {
        // Track data source files
        public static string Elevation_File { get; set; }
        public static string Curvature_File { get; set; }
        public static string Stations_File { get; set; }
        public static string Speedboards_File { get; set; }

        // Train data and Tractive Effort source file
        public static double Mass { get; set; }
        public static double Inertia { get; set; }
        public static double Overspeed_Limit { get; set; }
        public static double Brake_Decel { get; set; }
        public static int Brake_Delay { get; set; }
        public static double Train_Length { get; set; }
        public static string TE_File { get; set; }

    }
}
