﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using System.Drawing;
using System.Threading;
using IniParser;
using IniParser.Model;
using ScottPlot;
using NumSharp;
using MathNet.Numerics.Interpolation;

namespace MayoTrain
{
    public partial class MayoTrain : Form
    {

        private void ResetAxes()
        {
            // Reset various plot axes
            GradePlot.Plot.AxisAuto();
            CurvePlot.Plot.AxisAuto();

            //GradePlot.plt.XAxis(x1: 0);
            //CurvePlot.plt.XAxis(x1: 0);
        }

        public MayoTrain()
        {
            InitializeComponent();
            //this.Width = Screen.PrimaryScreen.WorkingArea.Width;
            //GradePlot.Width = Screen.PrimaryScreen.WorkingArea.Width-200;
        }

        public class TrackData
        {
            public double Start_KM { get; set; }
            public double End_KM { get; set; }
            public double Start_Z { get; set; }
            public double End_Z { get; set; }
        }

        private void LoadConfig(object sender, EventArgs e)
        {

            // Clear previous graph if reloading file
            if (btn_startSim.Enabled == true)
            {
                btn_startSim.Enabled = false;
                CurvePlot.Reset();
                GradePlot.Reset();
                CurvePlot.Visible = false;
                GradePlot.Visible = false;
            }

            string path;

            OpenFileDialog file = new OpenFileDialog();
            if (file.ShowDialog() == DialogResult.OK)
            {
                // Checks if config files is valid.
                // End of checks

                // Read configuration file
                var parser = new FileIniDataParser();
                parser.Parser.Configuration.KeyValueAssigmentChar = ':';
                parser.Parser.Configuration.SkipInvalidLines = true;
                IniData configData = parser.ReadFile(file.FileName);
                path = Path.GetDirectoryName(file.FileName);

                // Set global values
                Globals.Elevation_File = path + configData["Track_Data"]["Elevation_File"];
                Globals.Curvature_File = path + configData["Track_Data"]["Curvature_File"];
                Globals.Stations_File = path + configData["Track_Data"]["Stations_File"];
                Globals.Speedboards_File = path + configData["Track_Data"]["Speedboards_File"];
                Globals.TE_File = path + configData["Train_Data"]["TE_File"];
                Globals.Brake_Decel = Convert.ToDouble(configData["Train_Data"]["Brake_Deceleration"]);
                Globals.Brake_Delay = Convert.ToInt16(configData["Train_Data"]["Brake_Delay"]);
                Globals.Overspeed_Limit = Convert.ToDouble(configData["Train_Data"]["Overspeed_Limit"]);
                Globals.Train_Length = Convert.ToDouble(configData["Train_Data"]["Train_Length"]);

                // Set labels

                // Enable buttons to allow running of simulation
                btn_startSim.Enabled = true;
                checkbox_ShowPassedStations.Visible = true;
                checkBox_PlotAllGradeData.Visible = true;

            }
        }

        private void btn_startSim_Click(object sender, EventArgs e)
        {
            // Create Lists for data 
            List<string> Station_Name = new List<string>();
            List<double> Station_KM = new List<double>();
            List<bool> Station_Stop = new List<bool>();
            List<double> Grade_KM = new List<double>();
            List<double> Grade_Z = new List<double>();
            List<double> Radius_KM = new List<double>();
            List<double> Radius_Z = new List<double>();

            // Create plot variables
            double LastPlottedStationKM = 0;

            // Get data
            (Station_Name, Station_KM, Station_Stop) = TrainSim.LoadStationData(Globals.Stations_File);
            (Grade_KM, Grade_Z) = TrainSim.LoadTrackData(Globals.Elevation_File);
            (Radius_KM, Radius_Z) = TrainSim.LoadTrackData(Globals.Curvature_File);

            // Get grade interpolation function
            LinearSpline GradeInterpolation = LinearSpline.InterpolateSorted(Grade_KM.ToArray(), Grade_Z.ToArray());

            // Enable Plot controls
            btn_ResetAxes.Visible = true;

            // Reset Plots
            GradePlot.Visible = true;
            CurvePlot.Visible = true;
            GradePlot.Reset();
            CurvePlot.Reset();

            // Plot Data
            GradePlot.Plot.AddScatter(Grade_KM.ToArray(), Grade_Z.ToArray(), markerShape: MarkerShape.none);
            CurvePlot.Plot.AddScatter(Radius_KM.ToArray(), Radius_Z.ToArray(), markerShape: MarkerShape.none);

            // Plot styling

            // General
            ResetAxes();

            // Grade Plot
            GradePlot.Plot.Title("Height Data");
            GradePlot.Plot.YLabel("Z(m)");
            GradePlot.Plot.XLabel("KM");

            // Annotate stations if train stops there.
            for (int x = 0; x < Station_KM.Count(); x++)
            {
                var Station_Y = GradeInterpolation.Interpolate(Station_KM[x]);

                // Colour code stations if they are stopped at.
                if (Station_Stop[x] == true)
                {
                    GradePlot.Plot.AddPoint(Station_KM[x], Station_Y, color: Color.DarkBlue);
                    var AddedText = GradePlot.Plot.AddText(Station_Name[x], Station_KM[x], Station_Y, color: Color.DarkBlue);
                    AddedText.Rotation = -90;
                    AddedText.Alignment = Alignment.UpperLeft;
                    if (Station_KM[x] > LastPlottedStationKM) { LastPlottedStationKM = Station_KM[x]; } // Note last station to plot.
                }

                if ((Station_Stop[x] == false) & (checkbox_ShowPassedStations.Checked == true))
                {
                    GradePlot.Plot.AddPoint(Station_KM[x], Station_Y, color: Color.Gray);
                    var AddedText = GradePlot.Plot.AddText(Station_Name[x], Station_KM[x], Station_Y, color: Color.Gray);
                    AddedText.Rotation = -90;
                    AddedText.Alignment = Alignment.UpperLeft;
                }
            }

            // If all stations are being plotted, use the maximum.
            if (checkbox_ShowPassedStations.Checked == true) { LastPlottedStationKM = Station_KM.Max(); }

            if (checkBox_PlotAllGradeData.Checked == false)
            {
                // Limit the x-axis (KM) to the last stopping station + 2KM rounded.
                GradePlot.Plot.SetAxisLimitsX(0, LastPlottedStationKM + 2);
                CurvePlot.Plot.SetAxisLimits(0, LastPlottedStationKM + 2, 0);
            }
            else
            {
                // Plot all KM
                GradePlot.Plot.SetAxisLimits(0, null);
                CurvePlot.Plot.SetAxisLimits(0, null, 0);
                //GradePlot.Plot.AxisBounds(minX: 0);
                //CurvePlot.Plot.AxisBounds(minX: 0, minY: 0);
            }

            // Curve Plot
            CurvePlot.Plot.Title("Curve Data");
            CurvePlot.Plot.YLabel("Radius(m)");
            CurvePlot.Plot.XLabel("KM");

            // Render Plots
            GradePlot.Render();
            CurvePlot.Render();

            // Only required prior to plotting
            checkbox_ShowPassedStations.Visible = false;
            checkBox_PlotAllGradeData.Visible = false;
        }

        private void GradePlot_AxesChanged(object sender, EventArgs e)
        {
            // Change curve plot axes if grade plot changes.
            CurvePlot.Plot.MatchAxis(GradePlot.Plot, horizontal: true, vertical: false);
            CurvePlot.Render(skipIfCurrentlyRendering: true);
        }

        private void CurvePlot_AxesChanged(object sender, EventArgs e)
        {
            // Change grade plot axes if curve plot changes.
            GradePlot.Plot.MatchAxis(CurvePlot.Plot, horizontal: true, vertical: false);
            GradePlot.Render(skipIfCurrentlyRendering: true);
        }

        private void btn_ResetAxes_Click(object sender, EventArgs e)
        {
            ResetAxes();
            CurvePlot.Render(skipIfCurrentlyRendering: true);
            GradePlot.Render(skipIfCurrentlyRendering: true);
        }

    }
}
