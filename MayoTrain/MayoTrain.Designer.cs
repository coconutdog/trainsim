﻿namespace MayoTrain
{
    partial class MayoTrain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_loadConfig = new System.Windows.Forms.Button();
            this.btn_startSim = new System.Windows.Forms.Button();
            this.GradePlot = new ScottPlot.FormsPlot();
            this.CurvePlot = new ScottPlot.FormsPlot();
            this.btn_ResetAxes = new System.Windows.Forms.Button();
            this.checkbox_ShowPassedStations = new System.Windows.Forms.CheckBox();
            this.checkBox_PlotAllGradeData = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // btn_loadConfig
            // 
            this.btn_loadConfig.Location = new System.Drawing.Point(12, 12);
            this.btn_loadConfig.Name = "btn_loadConfig";
            this.btn_loadConfig.Size = new System.Drawing.Size(132, 23);
            this.btn_loadConfig.TabIndex = 2;
            this.btn_loadConfig.Text = "Load Configuration File";
            this.btn_loadConfig.UseVisualStyleBackColor = true;
            this.btn_loadConfig.Click += new System.EventHandler(this.LoadConfig);
            // 
            // btn_startSim
            // 
            this.btn_startSim.Enabled = false;
            this.btn_startSim.Location = new System.Drawing.Point(12, 44);
            this.btn_startSim.Name = "btn_startSim";
            this.btn_startSim.Size = new System.Drawing.Size(132, 23);
            this.btn_startSim.TabIndex = 4;
            this.btn_startSim.Text = "Start Simulation";
            this.btn_startSim.UseVisualStyleBackColor = true;
            this.btn_startSim.Click += new System.EventHandler(this.btn_startSim_Click);
            // 
            // GradePlot
            // 
            this.GradePlot.BackColor = System.Drawing.Color.Transparent;
            this.GradePlot.Location = new System.Drawing.Point(150, 44);
            this.GradePlot.Name = "GradePlot";
            this.GradePlot.Size = new System.Drawing.Size(1095, 622);
            this.GradePlot.TabIndex = 7;
            this.GradePlot.Visible = false;
            this.GradePlot.AxesChanged += new System.EventHandler(this.GradePlot_AxesChanged);
            // 
            // CurvePlot
            // 
            this.CurvePlot.BackColor = System.Drawing.Color.Transparent;
            this.CurvePlot.Location = new System.Drawing.Point(150, 672);
            this.CurvePlot.Name = "CurvePlot";
            this.CurvePlot.Size = new System.Drawing.Size(1095, 117);
            this.CurvePlot.TabIndex = 8;
            this.CurvePlot.Visible = false;
            this.CurvePlot.AxesChanged += new System.EventHandler(this.CurvePlot_AxesChanged);
            // 
            // btn_ResetAxes
            // 
            this.btn_ResetAxes.Location = new System.Drawing.Point(1113, 10);
            this.btn_ResetAxes.Name = "btn_ResetAxes";
            this.btn_ResetAxes.Size = new System.Drawing.Size(132, 23);
            this.btn_ResetAxes.TabIndex = 9;
            this.btn_ResetAxes.Text = "Reset Axes";
            this.btn_ResetAxes.UseVisualStyleBackColor = true;
            this.btn_ResetAxes.Visible = false;
            this.btn_ResetAxes.Click += new System.EventHandler(this.btn_ResetAxes_Click);
            // 
            // checkbox_ShowPassedStations
            // 
            this.checkbox_ShowPassedStations.AutoSize = true;
            this.checkbox_ShowPassedStations.Location = new System.Drawing.Point(150, 16);
            this.checkbox_ShowPassedStations.Name = "checkbox_ShowPassedStations";
            this.checkbox_ShowPassedStations.Size = new System.Drawing.Size(129, 17);
            this.checkbox_ShowPassedStations.TabIndex = 10;
            this.checkbox_ShowPassedStations.Text = "Show passed stations";
            this.checkbox_ShowPassedStations.UseVisualStyleBackColor = true;
            this.checkbox_ShowPassedStations.Visible = false;
            // 
            // checkBox_PlotAllGradeData
            // 
            this.checkBox_PlotAllGradeData.AutoSize = true;
            this.checkBox_PlotAllGradeData.Location = new System.Drawing.Point(280, 16);
            this.checkBox_PlotAllGradeData.Name = "checkBox_PlotAllGradeData";
            this.checkBox_PlotAllGradeData.Size = new System.Drawing.Size(111, 17);
            this.checkBox_PlotAllGradeData.TabIndex = 11;
            this.checkBox_PlotAllGradeData.Text = "Plot all grade data";
            this.checkBox_PlotAllGradeData.UseVisualStyleBackColor = true;
            this.checkBox_PlotAllGradeData.Visible = false;
            // 
            // MayoTrain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1269, 790);
            this.Controls.Add(this.checkBox_PlotAllGradeData);
            this.Controls.Add(this.checkbox_ShowPassedStations);
            this.Controls.Add(this.btn_ResetAxes);
            this.Controls.Add(this.CurvePlot);
            this.Controls.Add(this.GradePlot);
            this.Controls.Add(this.btn_startSim);
            this.Controls.Add(this.btn_loadConfig);
            this.Name = "MayoTrain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MayoTrain";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btn_loadConfig;
        private System.Windows.Forms.Button btn_startSim;
        private ScottPlot.FormsPlot GradePlot;
        private ScottPlot.FormsPlot CurvePlot;
        private System.Windows.Forms.Button btn_ResetAxes;
        private System.Windows.Forms.CheckBox checkbox_ShowPassedStations;
        private System.Windows.Forms.CheckBox checkBox_PlotAllGradeData;
    }
}

